const { merge } = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa-react");

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: "acid",
    projectName: "sspa-main",
    webpackConfigEnv,
    argv,
  });

  return merge(defaultConfig, {
    // Para evitar que se sature el inotify en linux
    watchOptions: {
      ignored: ['**/node_modules'],
    },
    module: {
      rules: [
        {
  
          test: /\.(woff|woff2|eot|ttf|otf)$/i,
          type: 'asset/resource',
  
        },
      ],
    },
  });
};
