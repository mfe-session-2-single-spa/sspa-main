import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";

import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";

export default function Root(props) {
  
   function publishCartEvent(data) {
    const event = new CustomEvent("cartAction", { detail: data });
    document.dispatchEvent(event);
  }
  
  return(
    <>
      <Box sx={{ "& button": { m: 1 } }}>
        <Typography variant="h3" gutterBottom>
          Products list
        </Typography>
        <Divider />
        <Typography variant="h5" gutterBottom>
          Add or Remove products to cart
        </Typography>
        <div>
          <Button onClick={() => publishCartEvent("add")} variant="contained" size="medium">
            Add
          </Button>
          <Button onClick={() => publishCartEvent()} variant="contained" size="medium">
            Remove
          </Button>
        </div>
      </Box>
    </>
  );
}
